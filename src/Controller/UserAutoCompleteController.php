<?php

namespace Drupal\contact_user\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Entity\Element\EntityAutocomplete;

/**
 * Defines a route controller for watches autocomplete form elements.
 */
class UserAutoCompleteController extends ControllerBase {

  /**
   * The node storage.
   *
   * @var \Drupal\node\NodeStorage
   */
  protected $nodeStorage;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->userStorage = $entity_type_manager->getStorage('user');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Handler for autocomplete request.
   */
  public function handleAutocomplete(Request $request) {
    $results = [];
    $input = $request->query->get('q');

    // Get the typed string from the URL, if it exists.
    if (!$input) {
      return new JsonResponse($results);
    }

    $input = Xss::filter($input);
    $query = $this->userStorage->getQuery();
    $group = $query
      ->orConditionGroup()
      ->condition('name', $input, 'CONTAINS')
      ->condition('mail', $input, 'CONTAINS');
    $query->condition('status', 1)
      ->condition($group)
      ->groupBy('uid')
      ->sort('created', 'DESC')
      ->range(0, 10);

    $ids = $query->execute();
    $users = $ids ? $this->userStorage->loadMultiple($ids) : [];

    foreach ($users as $user) {

      $label = [
        $user->name->value,
        '<small>(' . $user->mail->value . ')</small>',
      ];

      $results[] = [
        'value' => EntityAutocomplete::getEntityLabels([$user]),
        'label' => implode(' ', $label),
      ];
    }

    return new JsonResponse($results);
  }
}
