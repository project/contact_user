<?php

namespace Drupal\contact_user\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\message_notify\MessageNotifier;
use Drupal\message\Entity\Message;
use Drupal\Core\Entity\Element\EntityAutocomplete;
use Drupal\user\Entity\User;
use Drupal\Core\Logger\LoggerChannelFactory;

/**
 * Provides a Segovia Municipal Contact User form.
 */
class ContactForm extends FormBase {

  /**
   * The message notification service.
   *
   * @var \Drupal\message_notify\MessageNotifier
   */
  protected $messageNotifier;

  /**
   * Logger Factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct(MessageNotifier $message_notifier, LoggerChannelFactory $logger_factory) {
    $this->messageNotifier = $message_notifier;
    $this->loggerFactory = $logger_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('message_notify.sender'),
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'contact_user_contact';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['main'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Contact to user via email'),
    ];

    $form['main']['wrapper'] = [
      '#type' => 'container',
      '#attributes' => ['aria-expanded' => 'true'],
    ];

    $form['main']['wrapper']['intro'][] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t('The following form will send a message to the selected user.'),
    ];

    $form['main']['wrapper']['user'] = [
      '#type' => 'textfield',
      '#title' => 'Username',
      '#description' => $this->t('The user to send the message. Find a user by username or email.'),
      '#autocomplete_route_name' => 'contact_user.autocomplete.users',
      '#required' => TRUE,
    ];

    $form['main']['wrapper']['subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#required' => TRUE,
    ];

    $form['main']['wrapper']['message'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Message'),
      '#format' => 'full_html',
      '#required' => TRUE,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['main']['wrapper']['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (mb_strlen($form_state->getValue('message')['value']) < 10) {
      $form_state->setErrorByName('name', $this->t('Message should be at least 10 characters.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Extracts the entity ID from the autocompletion result.
    $user_id = EntityAutocomplete::extractEntityIdFromAutocompleteInput($form_state->getValue('user'));

    $message = Message::create(['template' => 'contact_user', 'uid' => $user_id]);
    $message->set('field_message_subject', $form_state->getValue('subject'));
    $message->set('field_message_body', $form_state->getValue('message'));
    $message->save();

    if ($message) {
      $this->messageNotifier->send($message, []);
//      $user = User::load($user_id);
//      $this->loggerFactory('contact_user')->notice("Email sended to " . $user->mail->value);
    }

    $this->messenger()->addStatus($this->t('The message has been sent.'));
    $form_state->setRedirect('contact_user.contact');
  }

}
