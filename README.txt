Contact User

This module allows site administrators to contact users registered in the system, quickly and easily.
Features:

    - Allows the site administrators to send emails to the registered users.
    - Allow the use of Html elements.
    - Search for users by username or email.
    - Customization of mail header and footer.

Dependencies:

This module depends on the following dependencies for its correct operation:

    - Mailsystem
    - Swiftmailer
    - Message
    - Message Notify

Usage

After module install and enabled you can customize your mail Header and Footer.
Access to your drupal administratión and go to Extend -> Locate or filter Contact User and click on Configure button,
this button also can be accessed by the following URL {your_site_url}/admin/structure/message. Once on this page,
you will see the "Contact User" message template, click on Edit and you can see and customize the Header and Footer sections.

For send messages to users go to People -> Contact User.

Select the user to send the message finding in the Username field, then fill the subject and the message and click on Send. That's all!

More info: https://www.drupal.org/project/contact_user